import base64


class CmdGpon:

    def command(self, username, password, type='UNREG', sn=None, slot=None, port=None, onu_id=None):
        username = base64.b64decode(username).decode()
        password = base64.b64decode(password).decode()
        cmd_gpon = ""
        if type.__eq__('UNREG'):
            cmd_gpon = f"""{username}
{password}
enable
scroll

config
display ont autofind all
quit
quit
y

"""

        elif type.__eq__('REG'):
            if sn is not None:
                cmd_gpon = f"""{username}
{password}
enable
scroll

config
display ont info by-sn {sn}
quit
quit
y

"""
            elif slot is not None or port is not None:
                if onu_id is not None:
                    cmd_gpon = f"""{username}
{password}
enable
scroll

config
display ont info 0 {slot} {port} {onu_id}
quit
quit
y

"""
                else:
                    cmd_gpon = f"""{username}
{password}
enable
scroll

config
display ont info 0 {slot} {port} all
quit
quit
y

"""

        elif type.__eq__('SERVICE'):
            cmd_gpon = f"""{username}
{password}
enable
scroll

config

display service-port port 0/{slot}/{port} ont {onu_id}

display ont wan-info 0/{slot} {port} {onu_id}

interface gpon 0/{slot}

display ont port state {port} {onu_id} pots-port all

quit

quit

quit
y

"""

        return cmd_gpon