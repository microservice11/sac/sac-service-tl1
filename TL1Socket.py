import socket
import time

from uvicorn.main import logger

import settings


class TL1Socket:
    def __init__(self, ip_server, port_tl1, timeout=120):
        self.__socket = None
        self.__error_msg = None
        self.connect(ip_server=ip_server, port_tl1=port_tl1, timeout=timeout)

    def connect(self, ip_server, port_tl1, timeout=120):
        self.__socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__socket.settimeout(int(timeout))
        try:
            if settings.DEBUG:
                logger.info(f"connect to nms server {ip_server} port {port_tl1}")
            self.__socket.connect((ip_server, port_tl1))

        except Exception as ex:
            self.__error_msg = f'Cannot connect to {ip_server} with port {port_tl1} : {ex}'
            raise Exception(self.__error_msg)

    def command(self, cmd, time_sleep=2, is_command_login=False):
        if settings.DEBUG and is_command_login is False:
            logger.info(cmd)
        self.__socket.send(str(cmd).encode())
        time.sleep(time_sleep)
        data = self.__socket.recv(80000).decode()
        if settings.DEBUG and is_command_login is False:
            logger.info(data)
        obj_data = self.__get_message_error(data)
        return {
            "status": obj_data['status'],
            "message": obj_data['message'],
            "command": cmd,
            "result": data,
        }

    def close(self):
        self.__socket.close()

    def __get_message_error(self, reacv: str):
        status = 'FAILED'
        if 'ENDESC=No error' in reacv or 'ENDESC=No Error' in reacv or 'ENDESC=""' in reacv or 'total_blocks' in reacv:
            status = "SUCCESS"
            pesan = 'No Error'

        elif 'EADD=object not exist, please check' in reacv or 'ENDESC=resource does not exist (ONU)' in reacv or 'EADD="Data not found, failed to get ONU AID, please check PONID, ONUID, MAC or LOID"' in reacv:
            pesan = 'ONU does not exist'

        elif 'ENDESC=invalid parameter format' in reacv or 'ENDESC=device does not exist' in reacv:
            pesan = 'IP GPON does not exist'

        elif 'INVALID SYNTAX OR PUNCTUATION' in reacv or 'ILLEGAL COMMAND CODE' in reacv or 'ENDESC=exception happensjava.lang.NullPointerException' in reacv:
            pesan = 'Format is Wrong'

        elif 'EADD=onu already exist' in reacv or 'EADD="ONU already exist"' in reacv or 'ENDESC=device operation failed (GPON ONU sn already exists.)' in reacv:
            pesan = 'ONU already exist'

        elif 'EADD=service not exist' in reacv:
            pesan = 'Service not exist'

        elif 'ENDESC=resource conflicts (NAME)' in reacv:
            pesan = 'ONU name already exist'

        elif 'EADD=server check input parameter error' in reacv:
            pesan = 'Bandwith profile does not exist'

        elif 'EADD=service already exist' in reacv:
            pesan = 'Service already exist'

        elif 'ENDESC=device operation failed (T-CONT name already exists.)' in reacv:
            pesan = 'Service Port already exist'

        elif 'ENDESC=resource does not exist (traffic profile)' in reacv:
            pesan = 'Upload profile does not exist'

        elif 'ENDESC=resource does not exist (bandwidth profile)' in reacv:
            pesan = 'Download profile does not exist'

        elif 'EADD=NGN User Data UserIndex Repeat' in reacv:
            pesan = 'Voice already exist'

        elif 'EADD=ngnvlan is not defined, please check' in reacv:
            pesan = 'Vlan voice does not exist'

        elif 'EADD=" SNMP operation failed or returned an error "' in reacv:
            pesan = 'Vlan or bandwith format does not exist'

        elif 'EADD=Can`t config the same VID when the connect type is the same' in reacv or 'EADD=only one INTERNET mode wanservice could be set on one port' in reacv:
            pesan = 'Wan COS must be set same with NMS'

        elif 'ENDESC=resource does not exist (NE topo node)' in reacv:
            pesan = 'IP GPON does not exist'

        elif 'ENDESC=invalid parameter format' in reacv or 'INVALID SYNTAX OR PUNCTUATION' in reacv or 'ILLEGAL COMMAND CODE' in reacv or 'ENDESC=exception happensjava.lang.NullPointerException' in reacv:
            pesan = 'There is something went wrong or invalid parameter format'

        else:
            pesan = 'There is something wrong'

        return {
            'status': status,
            'message': pesan
        }
