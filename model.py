from typing import Generic, TypeVar

from pydantic import BaseModel


class DtoNms(BaseModel):
    ip_server: str = None
    port_tl1: int = None
    vendor: str = None
    protocol: str = None
    username: str = None
    password: str = None


class DtoCmdDetail(BaseModel):
    target: str = None
    desc: str = None
    cmd: str = None
    cmds: list[str] = None
    skip_error: bool = None
    time_sleep: int = None


class DtoCmdBatchDetail(BaseModel):
    status: int = None
    ont: DtoCmdDetail = None
    inet: DtoCmdDetail = None
    voip: DtoCmdDetail = None
    iptv: DtoCmdDetail = None
    other: DtoCmdDetail = None


class DtoConfig(BaseModel):
    ip_gpon: str = None
    slot_port: str = None
    onu_id: str = None
    username: str = None
    password: str = None


class DtoCmd(BaseModel):
    nms: DtoNms = None
    login: str = None
    logout: str = None
    cmd_type: str = None
    cmd_list: DtoCmdDetail = DtoCmdDetail()
    gpon: list[DtoConfig] = [DtoConfig]
    ip_gpon: list[str] = None
    details: list[DtoCmdBatchDetail] = [DtoCmdBatchDetail]


class DtoProc(BaseModel):
    proccess_id: float = None
    configs: list[DtoCmd] = None
    username: str = None
    ip_client: str = None
    type: int = None
    type_req: int = None


class DtoLogDetail(BaseModel):
    id: float = None
    target: str = None
    action: str = None
    status: str = None
    cmd: str = None
    result: bytes = None
    log_config_id: str = None
    username: str = None
    create_at: str = None


class DtoLog(BaseModel):
    id: float = None
    username: str = None
    target: str = None
    action: str = None
    status: str = None
    type: int = None
    ip_client: str = None
    start_time: str = None
    stop_time: str = None


class DtoPayload(BaseModel):
    operation: str = None
    data: DtoLog = DtoLog()
