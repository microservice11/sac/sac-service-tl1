import settings

from fastapi import APIRouter

router = APIRouter(tags=['items'], responses={404: {"description": "Page Not Found"}})


@router.get("/")
async def root():
    return {
        "name": "sac-service-tl1",
        "version": settings.VERSION,
        "status": "up"
    }
