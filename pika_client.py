import asyncio
import json
from asyncio import AbstractEventLoop

from aio_pika import connect_robust, Channel, Message
from aio_pika.abc import AbstractRobustConnection, AbstractIncomingMessage
from aio_pika.pool import Pool

from processing_command import BatchViews
from model import DtoProc, DtoPayload, DtoLogDetail

EXCHANGE_LOG = 'log'
ROUTING_CONFIG = 'config'
QUEUE_LOG_CONFIG = f'{EXCHANGE_LOG}.logConfigQueue'
ROUTING_CONFIG_DETAIL = 'configDetail'
QUEUE_LOG_CONFIG_DETAIL = f'{EXCHANGE_LOG}.logConfigDetailQueue'

EXCHANGE_CONFIG = 'config'
ROUTING_DATA = 'data'
QUEUE_CONFIG_DATA = f'{EXCHANGE_CONFIG}.configDataQueue'

EXCHANGE_DLX = 'DLX'
ROUTING_DLX_LOG_CONFIG = QUEUE_LOG_CONFIG
QUEUE_DLX_LOG_CONFIG = f'{QUEUE_LOG_CONFIG}.dlq'
ROUTING_DLX_LOG_CONFIG_DETAIL = QUEUE_LOG_CONFIG_DETAIL
QUEUE_DLX_LOG_CONFIG_DETAIL = f'{QUEUE_LOG_CONFIG_DETAIL}.dlq'
ROUTING_DLX_DATA_CONFIG = QUEUE_CONFIG_DATA
QUEUE_DLX_DATA_CONFIG = f'{QUEUE_CONFIG_DATA}.dlq'


class PikaClient:

    def __init__(self, proccess_callable=None):
        self.loop = None
        self.proccess_callable = proccess_callable

    async def __get_connection(self) -> AbstractRobustConnection:
        return await connect_robust(host='rabbitmq', port=5672, login='guest', password='guest')

    def get_connection(self, loop: AbstractEventLoop) -> Pool:
        return Pool(self.__get_connection, max_size=2, loop=loop)

    def get_channel(self, loop: AbstractEventLoop) -> Pool:
        async def get_channel() -> Channel:
            async with self.get_connection(loop=loop).acquire() as connection:
                return await connection.channel()

        return Pool(get_channel, max_size=10, loop=loop)

    async def consume(self, loop: AbstractEventLoop) -> None:
        self.loop = loop
        async with self.get_channel(loop=loop).acquire() as chn:
            channel: Channel = chn
            await channel.set_qos(10)

            ###BIND DLX log.logConfigQueue
            exchange = await channel.declare_exchange(name=EXCHANGE_DLX, type='direct', durable=True)
            queue = await channel.declare_queue(name=QUEUE_DLX_LOG_CONFIG, durable=True)
            await queue.bind(exchange=exchange, routing_key=ROUTING_DLX_LOG_CONFIG)

            ###BIND log.logConfigQueue
            exchange = await channel.declare_exchange(name=EXCHANGE_LOG, type='topic', durable=True)
            queue = await channel.declare_queue(name=QUEUE_LOG_CONFIG, durable=True,
                                                arguments={'x-dead-letter-exchange': EXCHANGE_DLX,
                                                           'x-dead-letter-routing-key': QUEUE_LOG_CONFIG})
            await queue.bind(exchange=exchange, routing_key=ROUTING_CONFIG)

            ###BIND DLX log.logConfigDetailQueue
            exchange = await channel.declare_exchange(name=EXCHANGE_DLX, type='direct', durable=True)
            queue = await channel.declare_queue(name=QUEUE_DLX_LOG_CONFIG_DETAIL, durable=True)
            await queue.bind(exchange=exchange, routing_key=ROUTING_DLX_LOG_CONFIG_DETAIL)

            ###BIND log.logConfigDetailQueue
            exchange = await channel.declare_exchange(name=EXCHANGE_LOG, type='topic', durable=True)
            queue = await channel.declare_queue(name=QUEUE_LOG_CONFIG_DETAIL, durable=True,
                                                arguments={'x-dead-letter-exchange': EXCHANGE_DLX,
                                                           'x-dead-letter-routing-key': QUEUE_LOG_CONFIG_DETAIL})
            await queue.bind(exchange=exchange, routing_key=ROUTING_CONFIG_DETAIL)

            ###BIND DLX log.configDataQueue
            exchange = await channel.declare_exchange(name=EXCHANGE_DLX, type='direct', durable=True)
            queue = await channel.declare_queue(name=QUEUE_DLX_DATA_CONFIG, durable=True)
            await queue.bind(exchange=exchange, routing_key=ROUTING_DLX_DATA_CONFIG)

            ###BIND config.configDataQueue
            exchange = await channel.declare_exchange(name=EXCHANGE_CONFIG, type='topic', durable=True)
            queue = await channel.declare_queue(name=QUEUE_CONFIG_DATA, durable=True,
                                                arguments={'x-dead-letter-exchange': EXCHANGE_DLX,
                                                           'x-dead-letter-routing-key': QUEUE_CONFIG_DATA})
            await queue.bind(exchange=exchange, routing_key=ROUTING_DATA)

            await queue.consume(self.__process_message)

            try:
                await asyncio.Future()
            finally:
                async with self.get_connection(loop=loop).acquire() as conn:
                    connection: AbstractRobustConnection = conn
                    return await connection.close()

    async def __process_message(self, message: AbstractIncomingMessage) -> None:
        if self.loop is None:
            raise Exception("loop event is None")
        loop: AbstractEventLoop = self.loop
        async with message.process():
            dto_proc: DtoProc = DtoProc(**json.loads(message.body))
            await asyncio.sleep(1)
            batch = BatchViews()
            if dto_proc.type == 0 and dto_proc.type_req == 4:
                await batch.create(dto_proc, self.get_channel(loop))
            elif dto_proc.type == 0 and dto_proc.type_req == 0:
                await batch.check(dto_proc, self.get_channel(loop))
            elif dto_proc.type == 1 and dto_proc.type_req == 1:
                await batch.check_unreg_ssh(dto_proc, self.get_channel(loop))
            elif dto_proc.type == 1 and dto_proc.type_req == 2:
                await batch.check_reg_ssh(dto_proc, self.get_channel(loop))
            elif dto_proc.type == 1 and dto_proc.type_req == 3:
                await batch.check_service_ssh(dto_proc, self.get_channel(loop))

    async def publish(self, message, routing_key: str, channel_pool: Pool):
        if isinstance(message, (DtoPayload, DtoLogDetail)):
            async with channel_pool.acquire() as chn:
                channel: Channel = chn
                exchange = await channel.get_exchange(name=EXCHANGE_LOG)
                payload = json.dumps(message.dict())
                await exchange.publish(message=Message(payload.encode()), routing_key=routing_key)

