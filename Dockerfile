FROM gtxiqbal4559/microservices:python
ENV PYTHONUNBUFFERED=1
RUN mkdir /opt/sac-service-tl1
COPY . /opt/sac-service-tl1
WORKDIR /opt/sac-service-tl1
RUN pip install --no-cache-dir -r requirements.txt
EXPOSE 8002
ENTRYPOINT ["gunicorn", "main:app", "--workers", "2", "--worker-class", "uvicorn.workers.UvicornWorker", "--bind", ":8002", "--timeout", "250"]
