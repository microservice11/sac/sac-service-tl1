import base64
import time
from datetime import datetime

import paramiko
from aio_pika.pool import Pool
from uvicorn.main import logger

import pika_client
from CmdGpon import CmdGpon
from TL1Socket import TL1Socket
from model import DtoProc, DtoCmd, DtoPayload, DtoLogDetail, DtoConfig


class BatchViews:

    def __init__(self) -> None:
        super().__init__()

    async def __send_log_config_check(self, message: DtoPayload, status='SUCCESS', channel_pool: Pool = None):
        message.data.status = status
        message.data.stop_time = f'{datetime.now()}'[0:23]
        message.operation = 'update'
        pika_cl = pika_client.PikaClient()
        await pika_cl.publish(message=message, routing_key='config', channel_pool=channel_pool)

    async def __send_log_detail_config_check(self, message: DtoLogDetail, channel_pool: Pool):
        message.create_at = f'{datetime.now()}'[0:23]
        pika_cl = pika_client.PikaClient()
        await pika_cl.publish(message=message, routing_key='configDetail', channel_pool=channel_pool)

    async def __get_log(self, request: DtoProc, action: str, target=None, process_id: float = None, type: int = 0,
                        channel_pool: Pool = None) -> DtoPayload:
        payload: DtoProc = request

        target_list: list[str] = []
        for config in payload.configs:
            data: DtoCmd = config

            target_list.append(data.nms.ip_server)

        if len(target_list) > 0:
            target = f"IP NMS: {','.join(target_list)}"

        username = request.username
        ip_client = request.ip_client
        now = datetime.now()
        dt_obj = datetime.strptime(now.strftime("%d.%m.%Y %H:%M:%S,%f"), "%d.%m.%Y %H:%M:%S,%f")
        log_id: float = dt_obj.timestamp() * 1000000 if process_id is None else process_id

        param_data: DtoPayload = DtoPayload()
        param_data.operation = 'insert'
        param_data.data.id = log_id
        param_data.data.target = target
        param_data.data.action = action
        param_data.data.status = 'IN_PROGRESS'
        param_data.data.ip_client = ip_client
        param_data.data.username = username
        param_data.data.type = type
        param_data.data.start_time = f'{now}'[0:23]

        pika_cl = pika_client.PikaClient()
        await pika_cl.publish(message=param_data, routing_key='config', channel_pool=channel_pool)
        return param_data

    async def __get_log_detail(self, payload: DtoLogDetail, target, result, channel_pool: Pool):
        payload.target = target
        payload.cmd = result['command']
        payload.result = result['result']
        await self.__send_log_detail_config_check(payload, channel_pool)

    async def check_unreg_ssh(self, request: DtoProc, channel_pool: Pool):
        log_data = None
        data_list: DtoProc = request
        cmd_gpon = CmdGpon()
        for dto_cmd_check in data_list.configs:
            data: DtoCmd = dto_cmd_check
            ip_server = data.nms.ip_server
            port_tl1 = data.nms.port_tl1
            username = data.nms.username
            password = data.nms.password
            gpons = data.gpon
            ssh_client = paramiko.SSHClient()
            ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

            if log_data is None:
                log_data = await self.__get_log(request, 'CEK SN UNREG/REG/SERVICE', None, data_list.proccess_id, 1,
                                                channel_pool)

            message: DtoLogDetail = DtoLogDetail()
            message.target = None
            message.action = 'CEK UNREGISTER ONT'
            message.status = 'IN_PROGRESS'
            message.cmd = 'display ont autofind all'
            message.result = None
            message.username = request.username
            message.log_config_id = log_data.data.id

            try:
                ssh_client.connect(hostname=ip_server, port=port_tl1, username=base64.b64decode(username).decode(),
                                   password=base64.b64decode(password).decode(), look_for_keys=False)
            except Exception as e:
                hasil = f"timeout for connecting {ip_server} port {port_tl1}"
                message.target = f"IP NMS: {ip_server}"
                message.result = hasil
                message.status = 'FAILED'
                await self.__send_log_detail_config_check(message, channel_pool)
                await self.__send_log_config_check(log_data, message.status, channel_pool)
                logger.error(hasil)
                return False

            for gpon in gpons:
                command = f"telnet {gpon.ip_gpon}"
                logger.info(command)
                stdin, stdout, stderr = ssh_client.exec_command(command)
                time.sleep(2)
                if gpon.username is not None or gpon.password is not None:
                    username = gpon.username
                    password = gpon.password
                cmd = cmd_gpon.command(username=username, password=password, type="UNREG")
                stdin.write(cmd)
                hasil = stdout.read().decode()
                logger.info(hasil)
                message.target = f"IP GPON: {gpon.ip_gpon}"
                message.result = hasil
                message.status = 'SUCCESS'
                await self.__send_log_detail_config_check(message, channel_pool)

            ssh_client.exec_command("logout")
            ssh_client.close()

        await self.__send_log_config_check(log_data, channel_pool=channel_pool)
        return True

    async def check_reg_ssh(self, request: DtoProc, channel_pool: Pool):
        log_data = None
        data_list: DtoProc = request
        cmd_gpon = CmdGpon()
        for dto_cmd_check in data_list.configs:
            data: DtoCmd = dto_cmd_check
            ip_server = data.nms.ip_server
            port_tl1 = data.nms.port_tl1
            username = data.nms.username
            password = data.nms.password
            gpons = data.gpon
            ssh_client = paramiko.SSHClient()
            ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

            if log_data is None:
                log_data = await self.__get_log(request, 'CEK SN UNREG/REG/SERVICE', None, data_list.proccess_id, 1,
                                                channel_pool)

            message: DtoLogDetail = DtoLogDetail()
            message.target = None
            message.action = 'CEK REGISTER ONT'
            message.status = 'IN_PROGRESS'
            message.cmd = None
            message.result = None
            message.username = request.username
            message.log_config_id = log_data.data.id

            try:
                ssh_client.connect(hostname=ip_server, port=port_tl1, username=base64.b64decode(username).decode(),
                                   password=base64.b64decode(password).decode(), look_for_keys=False)
            except Exception as e:
                hasil = f"timeout for connecting {ip_server} port {port_tl1}"
                message.target = f"IP NMS: {ip_server}"
                message.result = hasil
                message.status = 'FAILED'
                await self.__send_log_detail_config_check(message, channel_pool)
                await self.__send_log_config_check(log_data, message.status, channel_pool)
                logger.error(hasil)
                return False

            for dto_config in gpons:
                gpon: DtoConfig = dto_config
                command = f"telnet {gpon.ip_gpon}"
                logger.info(command)
                stdin, stdout, stderr = ssh_client.exec_command(command)
                time.sleep(2)
                slot = gpon.slot_port.split("-")[0] if gpon.slot_port is not None else None
                port = gpon.slot_port.split("-")[1] if gpon.slot_port is not None else None
                sn = gpon.onu_id
                if gpon.username is not None or gpon.password is not None:
                    username = gpon.username
                    password = gpon.password
                if sn is not None and len(sn) > 2:
                    message.target = f"IP GPON: {gpon.ip_gpon}, SN: {sn}"
                    message.cmd = f"display ont info by-sn {sn}"
                    cmd = cmd_gpon.command(username=username, password=password, type="REG", sn=sn)
                else:
                    onu_id = gpon.onu_id
                    if onu_id is not None:
                        message.target = f"IP GPON: {gpon.ip_gpon}, SLOT: {slot}, PORT: {port}, ONUID: {onu_id}"
                        message.cmd = f"display ont info 0 {slot} {port} {onu_id}"
                        cmd = cmd_gpon.command(username=username, password=password, type="REG", slot=slot, port=port,
                                               onu_id=onu_id)
                    else:
                        message.target = f"IP GPON: {gpon.ip_gpon}, SLOT: {slot}, PORT: {port}"
                        message.cmd = f"display ont info 0 {slot} {port} all"
                        cmd = cmd_gpon.command(username=username, password=password, type="REG", slot=slot, port=port)

                stdin.write(cmd)
                hasil = stdout.read().decode()
                logger.info(hasil)
                message.result = hasil
                message.status = 'SUCCESS'
                await self.__send_log_detail_config_check(message, channel_pool)

            ssh_client.exec_command("logout")
            ssh_client.close()

        await self.__send_log_config_check(log_data, channel_pool=channel_pool)
        return True

    async def check_service_ssh(self, request: DtoProc, channel_pool: Pool):
        log_data = None
        data_list: DtoProc = request
        cmd_gpon = CmdGpon()
        for dto_cmd_check in data_list.configs:
            data: DtoCmd = dto_cmd_check
            ip_server = data.nms.ip_server
            port_tl1 = data.nms.port_tl1
            username = data.nms.username
            password = data.nms.password
            gpons = data.gpon
            ssh_client = paramiko.SSHClient()
            ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())

            if log_data is None:
                log_data = await self.__get_log(request, 'CEK SN UNREG/REG/SERVICE', None, data_list.proccess_id, 1,
                                                channel_pool)

            message: DtoLogDetail = DtoLogDetail()
            message.target = None
            message.action = 'CEK SERVICE ONT'
            message.status = 'IN_PROGRESS'
            message.cmd = None
            message.result = None
            message.username = request.username
            message.log_config_id = log_data.data.id

            try:
                ssh_client.connect(hostname=ip_server, port=port_tl1, username=base64.b64decode(username).decode(),
                                   password=base64.b64decode(password).decode(), look_for_keys=False)
            except Exception as e:
                hasil = f"timeout for connecting {ip_server} port {port_tl1}"
                message.target = f"IP NMS: {ip_server}"
                message.result = hasil
                message.status = 'FAILED'
                await self.__send_log_detail_config_check(message, channel_pool)
                await self.__send_log_config_check(log_data, message.status, channel_pool)
                logger.error(hasil)
                return False

            cmd = ''
            for dto_config in gpons:
                gpon: DtoConfig = dto_config
                command = f"telnet {gpon.ip_gpon}"
                logger.info(command)
                stdin, stdout, stderr = ssh_client.exec_command(command)
                time.sleep(2)
                slot = gpon.slot_port.split("-")[0] if gpon.slot_port is not None else None
                port = gpon.slot_port.split("-")[1] if gpon.slot_port is not None else None
                if gpon.username is not None or gpon.password is not None:
                    username = gpon.username
                    password = gpon.password
                if slot is not None or port is not None:
                    onu_id = gpon.onu_id
                    if onu_id is not None:
                        message.target = f"IP GPON: {gpon.ip_gpon}, SLOT: {slot}, PORT: {port}, ONUID: {onu_id}"
                        message.cmd = f"display service-port port 0/{slot}/{port} ont {onu_id},display ont wan-info 0/{slot} {port} {onu_id},interface gpon 0/{slot},display ont port state {port} {onu_id} pots-port all"
                        cmd = cmd_gpon.command(username=username, password=password, type="SERVICE", slot=slot,
                                               port=port,
                                               onu_id=onu_id)

                stdin.write(cmd)
                hasil = stdout.read().decode()
                logger.info(hasil)
                message.result = hasil
                message.status = 'SUCCESS'
                await self.__send_log_detail_config_check(message, channel_pool)

            ssh_client.exec_command("logout")
            ssh_client.close()

        await self.__send_log_config_check(log_data, channel_pool=channel_pool)
        return True

    async def check(self, request: DtoProc, channel_pool: Pool):
        log_data = None
        status = 'SUCCESS'
        data_list = request
        for dto_cmd_check in data_list.configs:
            data: DtoCmd = dto_cmd_check
            ip_server = data.nms.ip_server
            port_tl1 = data.nms.port_tl1
            cmd_login = data.login
            cmd_logout = data.logout
            cmd_list = data.cmd_list.cmds
            time_sleep = data.cmd_list.time_sleep

            if log_data is None:
                log_data = await self.__get_log(request, 'CEK SN UNREG/REG/SERVICE', None, data_list.proccess_id, 1,
                                                channel_pool)

            message: DtoLogDetail = DtoLogDetail()
            message.target = None
            message.action = data.cmd_list.desc
            message.status = 'IN_PROGRESS'
            message.cmd = None
            message.result = None
            message.username = request.username
            message.log_config_id = log_data.data.id

            try:
                config = TL1Socket(ip_server=ip_server, port_tl1=port_tl1)

                if cmd_login is not None:
                    reacv = config.command(cmd_login, is_command_login=True)
                    if reacv['status'].__eq__('FAILED'):
                        result = {
                            "command": 'LOGIN:::CTAG::UN=******,PWD=******;',
                            "result": reacv['result'],
                        }

                        cmd_split = cmd_login.split(',')
                        target = f"IP GPON: {cmd_split[0].split('::')[1].split('=')[1]}"
                        if len(cmd_split) > 1:
                            slot_port = cmd_split[1].split('=')[1].split('-')
                            slot = slot_port[2]
                            port = slot_port[3].split(":::;")
                            target += f", SLOT: {slot}, PORT: {port[0]}"

                        if len(cmd_split) > 2:
                            onu_id = cmd_split[3].split('=')[1].split(':::;')[0]
                            target += f", SN: {onu_id}"

                        message.status = reacv['status']
                        message.target = target
                        message.cmd = result['command']
                        message.result = reacv['result']

                        await self.__send_log_detail_config_check(message, channel_pool)
                        config.close()
                        await self.__send_log_config_check(log_data, 'FAILED', channel_pool)
                        return False

                for cmd in cmd_list:
                    reacv = config.command(cmd, time_sleep)
                    result = {
                        "command": reacv['command'],
                        "result": reacv['result'],
                    }

                    cmd_split = cmd.split(',')
                    target = f"IP GPON: {cmd_split[0].split('::')[1].split('=')[1]}"
                    if len(cmd_split) > 1:
                        slot_port = cmd_split[1].split('=')[1].split('-')
                        slot = slot_port[2]
                        port = slot_port[3].split(":::;")
                        target += f", SLOT: {slot}, PORT: {port[0]}"

                    if len(cmd_split) > 2:
                        onu_id = cmd_split[3].split('=')[1].split(':::;')[0]
                        target += f", SN: {onu_id}"

                    message.status = reacv['status']
                    message.target = target
                    message.cmd = cmd
                    message.result = reacv['result']

                    await self.__send_log_detail_config_check(message, channel_pool)

                    if reacv['status'].__eq__('FAILED'):
                        if cmd_logout is not None:
                            config.command(cmd_logout, is_command_login=True)

                        config.close()
                        await self.__send_log_config_check(log_data, 'FAILED', channel_pool)
                        return False

                if cmd_logout is not None:
                    config.command(cmd_logout, is_command_login=True)

                config.close()
            except Exception as ex:
                status = 'FAILED'
                message.status = status
                message.target = f'IP SERVER: {ip_server}'
                message.cmd = f'telnet {ip_server}:{port_tl1}'
                message.result = ex.__str__()
                msg = message.result
                logger.error(msg)
                await self.__send_log_detail_config_check(message, channel_pool)

        await self.__send_log_config_check(log_data, status, channel_pool)
        return True

    async def create(self, request: DtoProc, channel_pool: Pool):
        datas: DtoProc = request

        status = 'SUCCESS'
        log_data = await self.__get_log(request, 'CONFIG', None, datas.proccess_id, 1, channel_pool)

        message: DtoLogDetail = DtoLogDetail()
        message.target = None
        message.action = 'Connect to Server'
        message.status = 'IN_PROGRESS'
        message.cmd = None
        message.result = None
        message.username = request.username
        message.log_config_id = log_data.data.id

        ip_server = ''
        port_tl1 = 0
        try:
            for dto_cmd_batch in datas.configs:
                data: DtoCmd = dto_cmd_batch
                ip_server = data.nms.ip_server
                port_tl1 = data.nms.port_tl1
                cmd_login = data.login
                cmd_logout = data.logout
                details = data.details

                config = TL1Socket(ip_server=ip_server, port_tl1=port_tl1)

                if cmd_login is not None:
                    reacv = config.command(cmd_login, is_command_login=True)
                    if reacv['status'].__eq__('FAILED'):
                        message.action = 'Login Failed'
                        result = {
                            "command": 'LOGIN:::CTAG::UN=******,PWD=******;',
                            "result": reacv['result'],
                        }
                        message.status = reacv['status']
                        await self.__get_log_detail(message, 'Login', result, channel_pool)
                        config.close()
                        await self.__send_log_config_check(log_data, 'FAILED', channel_pool)
                        return False

                for detail in details:
                    sts_cfg = detail.status
                    ont = detail.ont
                    inet = detail.inet
                    voip = detail.voip
                    iptv = detail.iptv
                    other = detail.other

                    cmd_ont = None
                    if ont is not None:
                        cmd_ont = ont.cmd

                    cmd_inet = None
                    if inet is not None:
                        cmd_inet = inet.cmds

                    cmd_voip = None
                    if voip is not None:
                        cmd_voip = voip.cmds

                    cmd_iptv = None
                    if iptv is not None:
                        cmd_iptv = iptv.cmds

                    cmd_other = None
                    if other is not None:
                        cmd_other = other.cmds

                    i = 1
                    if cmd_ont is not None and sts_cfg == 1:
                        reacv = config.command(cmd_ont)
                        message.action = detail.ont.desc
                        result = {
                            "command": reacv['command'],
                            "result": reacv['result'],
                        }
                        message.status = reacv['status']
                        await self.__get_log_detail(message, detail.ont.target, result, channel_pool)
                        i += i
                        if reacv['status'].__eq__('FAILED'):
                            if 'ADD-ONU' in cmd_ont:
                                if cmd_logout is not None:
                                    config.command(cmd_logout, is_command_login=True)

                                config.close()
                                await self.__send_log_config_check(log_data, 'FAILED', channel_pool)
                                return False

                    i = 1
                    if cmd_inet is not None:
                        message.action = detail.inet.desc
                        for cmd in cmd_inet:
                            reacv = config.command(cmd)
                            result = {
                                "command": reacv['command'],
                                "result": reacv['result'],
                            }
                            message.status = reacv['status']
                            await self.__get_log_detail(message, detail.inet.target, result, channel_pool)
                            i += 1
                            if reacv['status'].__eq__('FAILED') and not detail.inet.skip_error:
                                if cmd_logout is not None:
                                    config.command(cmd_logout, is_command_login=True)

                                config.close()
                                await self.__send_log_config_check(log_data, 'FAILED', channel_pool)
                                return False

                    i = 1
                    if cmd_voip is not None:
                        message.action = detail.voip.desc
                        for cmd in cmd_voip:
                            reacv = config.command(cmd)
                            result = {
                                "command": reacv['command'],
                                "result": reacv['result'],
                            }
                            message.status = reacv['status']
                            await self.__get_log_detail(message, detail.voip.target, result, channel_pool)
                            i += 1
                            if reacv['status'].__eq__('FAILED') and not detail.voip.skip_error:
                                if cmd_logout is not None:
                                    config.command(cmd_logout, is_command_login=True)

                                config.close()
                                await self.__send_log_config_check(log_data, 'FAILED', channel_pool)
                                return False

                    i = 1
                    if cmd_iptv is not None:
                        message.action = detail.iptv.desc
                        for cmd in cmd_iptv:
                            reacv = config.command(cmd)
                            result = {
                                "command": reacv['command'],
                                "result": reacv['result'],
                            }
                            message.status = reacv['status']
                            await self.__get_log_detail(message, detail.iptv.target, result, channel_pool)
                            i += 1
                            if reacv['status'].__eq__('FAILED') and not detail.iptv.skip_error:
                                if cmd_logout is not None:
                                    config.command(cmd_logout, is_command_login=True)

                                config.close()
                                await self.__send_log_config_check(log_data, 'FAILED', channel_pool)
                                return False

                    i = 1
                    if cmd_other is not None:
                        message.action = detail.other.desc
                        for cmd in cmd_other:
                            reacv = config.command(cmd)
                            result = {
                                "command": reacv['command'],
                                "result": reacv['result'],
                            }
                            message.status = reacv['status']
                            await self.__get_log_detail(message, detail.other.target, result, channel_pool)
                            i += 1
                            if reacv['status'].__eq__('FAILED') and not detail.other.skip_error:
                                if cmd_logout is not None:
                                    config.command(cmd_logout, is_command_login=True)

                                config.close()
                                await self.__send_log_config_check(log_data, 'FAILED', channel_pool)
                                return False

                    i = 1
                    if cmd_ont is not None and sts_cfg == 2:
                        reacv = config.command(cmd_ont)
                        message.action = detail.ont.desc
                        result = {
                            "command": reacv['command'],
                            "result": reacv['result'],
                        }
                        message.status = reacv['status']
                        await self.__get_log_detail(message, detail.ont.target, result, channel_pool)
                        i += i
                        if reacv['status'].__eq__('FAILED'):
                            if 'ADD-ONU' in cmd_ont:
                                if cmd_logout is not None:
                                    config.command(cmd_logout, is_command_login=True)

                                config.close()
                                await self.__send_log_config_check(log_data, 'FAILED', channel_pool)
                                return False

                if cmd_logout is not None:
                    config.command(cmd_logout, is_command_login=True)

                config.close()

        except Exception as ex:
            status = 'FAILED'
            message.status = status
            message.target = f'IP SERVER: {ip_server}'
            message.cmd = f'telnet {ip_server}:{port_tl1}'
            message.result = ex.__str__()
            logger.error(message.result)
            await self.__send_log_detail_config_check(message, channel_pool)

        await self.__send_log_config_check(log_data, status, channel_pool)
        return True
