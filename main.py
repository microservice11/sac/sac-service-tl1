import asyncio
import settings

from fastapi import FastAPI
from uvicorn.main import logger

from pika_client import PikaClient
from router import router


class PikaApp(FastAPI):

    def __init__(self, *args, **kwargs):
        super(PikaApp, self).__init__(*args, **kwargs)
        self.pika_client = PikaClient(self.incoming_message)

    @classmethod
    def incoming_message(cls, message: dict):
        print(f'Incoming message {message}')


app = PikaApp()
app.include_router(router)

logger.info(f'sac-service-tl1 {settings.VERSION}')


@app.on_event('startup')
async def startup():
    loop = asyncio.get_event_loop()
    # task = loop.create_task(app.pika_client.consume(loop))
    # await task
    asyncio.ensure_future(app.pika_client.consume(loop))
